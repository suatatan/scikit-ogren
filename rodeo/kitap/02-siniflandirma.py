from sklearn.datasets import fetch_mldata
mnist = fetch_mldata('MNIST original')
X,y=mnist['data'],mnist['target']

import matplotlib
import matplotlib.pyplot as plt

dijital_veri=X[36000]
print(dijital_veri)
dijital_veri_resmi=dijital_veri.reshape(28,28)
plt.imshow(dijital_veri_resmi,cmap=matplotlib.cm.binary,interpolation='nearest')
plt.axis('off')
plt.show()

y[36000]

import numpy as np

X_train=X[:60000]
x_test=X[60000:]
y_train=y[:60000]
y_test=y[60000:]

#training data üzerinden shuffle_index

shuffle_index=np.random.permutation(60000)
X_train=X_train[shuffle_index]
y_train=y_train[shuffle_index]

#5 mi değil mi deneyi

y_train_5=(y_train == 5)
y_test_5 =(y_test == 5)


#Stokastik Gradyan Deseni

from sklearn.linear_model import SGDClassifier
sgd_clf=SGDClassifier(random_state=42)
sgd_clf.fit(X_train,y_train_5)

# bil bakalım

sgd_clf.predict([dijital_veri])

# başka deneme. 66.666 sayı. 
y[66666]
# 6 imiş bakalım algoritma onun 5 olup olmadığını anlayacak mı?
sgd_clf.predict([X[66666]])
# makine de 5 değil dedi.

#Accuracy
#Stratified k-fold ile accuracy hesabı
from sklearn.model_selection import cross_val_score
cross_val_score(sgd_clf,X_train,y_train,cv=3,scoring='accuracy')
# %87

#Confusion Matrix
from sklearn.model_selection import cross_val_predict
from sklearn.metrics import confusion_matrix

y_train_pred=cross_val_predict(sgd_clf,X_train,y_train_5,cv=3)
confusion_matrix(y_train_5,y_train_pred)

#Precision

from sklearn.metrics import precision_score,recall_score,f1_score
precision_score(y_train_5,y_train_pred)

# Recall

recall_score(y_train_5,y_train_pred)

# F1 score

f1_score(y_train_5,y_train_pred)