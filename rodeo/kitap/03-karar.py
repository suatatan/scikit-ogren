#Scikit ile gelen random veri tabanlarını kullanarak karar ağacı ml algoritmasını deneyelim
from sklearn.datasets import make_moons
from skler
import seaborn as sns 
import matplotlib.pyplot as plt
import pandas as pd
%matplotlib inline
#-----------------------------------------------------------------------------------------------
#1 Veriyi getir
#verimize noise katmak da mümkün
moons_data,moons_label=make_moons(n_samples=10000,noise=0.4)
#oluşacak grafikte iç içe geçmiş iki ay şeklinde veriler var. noiseu azaltırsak tam ay olur.
plt.scatter(moons_data[:,0],
            moons_data[:,1], 
            c = moons_label,
            cmap='viridis')
moons_df = pd.DataFrame({'x':moons_data[:,0],'y':moons_data[:,1],'Label':moons_label})
moons_df.head(2)
#-----------------------------------------------------------------------------------------------
#1 Eğitim/Test ayrımı
from sklearn.model_selection import train_test_split
X,y=moons_data, moons_label
X_train,X_test,y_train,y_test=train_test_split(X,y,
                                                random_state=42,
                                                shuffle=True,
                                                test_size=0.25)
#-----------------------------------------------------------------------------------------------
#2 Grid Search
from sklearn.model_selection import GridSearchCV
param_grid = [{'max_depth': [2,3,4,5]}]
#-----------------------------------------------------------------------------------------------
#3 Model
from sklearn.tree import DecisionTreeClassifier
tree_clf=DecisionTreeClassifier(max_depth=2)
tree_clf.fit(X_train,y_train)
#grid search ekle
tree_clf_grid=GridSearchCV(tree_clf,param_grid,cv=5,verbose=3)
tree_clf_grid.fit(X_train,y_train)
tree_clf_grid.best_params_
#max depth 2 en iyi seçimmiş


#-----------------------------------------------------------------------------------------------
#3 Performans
#3.1 Training set üzerinden
from sklearn.model_selection import cross_val_score
scores = cross_val_score(tree_clf, X_train, y_train, cv=10)
scores.mean()
#3.1 Test set üzerinden
scores = cross_val_score(tree_clf, X_test, y_test, cv=10)
scores.mean()


#-----------------------------------------------------------------------------------------------
#3 Decision tree çizdir

