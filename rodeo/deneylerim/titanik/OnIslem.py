# -*- coding: utf-8 -*-
import os
import pandas as pd
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import Imputer
import imp as im
"""
class OnIslem:
    def __init__(self):
        pass 
    def yap(self):
"""

dosyaYolu="C:\\Users\\suata\\Desktop\\scikit-ogren\\rodeo\\deneylerim\\titanik\\"
dosyaAdi="titanic_train.csv"
train_data = pd.read_csv(dosyaYolu+dosyaAdi)
train_data.info()
train_data.describe()
train_data['Sex'].value_counts()

"""
imp.load_source('module.name', dosyaYolu+'Deneme.py')
t=Deneme()
t.yap()
"""

#TODO: Aşağıdaki kısım bu veri seti için özel denemedir. Kaldırabilirsin
train_data['RelativesOnboard']= train_data["SibSp"] + train_data["Parch"]
train_data['RelativesOnboardNorm']=train_data["RelativesOnboard"]/sum(train_data["RelativesOnboard"])

imw= train_data['Name'].str.contains("Mrs")
train_data['isMarriedWoman']=imw.astype("int") 

imu= train_data['Name'].str.contains("Miss")
train_data['isSingleWoman']=imu.astype("int") 

import seaborn as sns
corr = train_data.corr()
sns.heatmap(corr, 
            xticklabels=corr.columns.values,
            yticklabels=corr.columns.values)



ce= im.load_source('module.name', dosyaYolu+'CategoricalEncoder.py')
dfs=im.load_source('module.name', dosyaYolu+'DataFrameSelector.py')
mfi=im.load_source('module.name', dosyaYolu+'MostFrequentImputer.py')

#Nümerik değerlerin seçimi ve doldurma işlemleri
#TODO 1: Hangi değerler kategorik ise select_numeric kısmında adlarını ver
#TODO 2: Eğer eksik hücre yoksa imputer pipeline'ını kaldır.
imputer = Imputer(strategy="median")
num_pipeline = Pipeline([
        ("select_numeric", dfs.DataFrameSelector(["Age", "SibSp", "Parch", "Fare"])),
        ("imputer", Imputer(strategy="median")),
    ])
#num_pipeline.fit_transform(train_data)

# Sayısal değerler için boş değerleri en sık değerlerle değiştiren dönüştürücü ve Kategori dönüştürücüsü
#TODO 3: Kategorik değerleri seç ve select_cat kısmına yaz
#TODO 4: imputer gerekşir mi kontrol et
cat_pipeline = Pipeline([
        ("select_cat", dfs.DataFrameSelector(["Pclass","Sex", "Embarked"])),
        ("imputer", mfi.MostFrequentImputer()),
        ("cat_encoder", ce.CategoricalEncoder(encoding='onehot-dense')),
    ])
#cat_pipeline.fit_transform(train_data)        
#Pipeline'ları birleştir
from sklearn.pipeline import FeatureUnion
preprocess_pipeline = FeatureUnion(transformer_list=[
        ("num_pipeline", num_pipeline),
        ("cat_pipeline", cat_pipeline),
    ])
#dönüştürülmüş veri
X_train = preprocess_pipeline.fit_transform(train_data)
y_train = train_data["Survived"]
#        return X_train,y_train







