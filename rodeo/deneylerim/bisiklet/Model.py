#Run order:3
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import SGDRegressor
from sklearn.metrics import mean_squared_error
%matplotlib inline
import matplotlib
import matplotlib.pyplot as plt


#önce ön işlem modülünü çalıştır sonra buraya gel denemek için a_prepared e bak varsa tamam

#model

lin_reg = LinearRegression()
#daha önce hazırladığımız a_prepared ve a_labels değişkenlerimizi kullanıyoruz
lin_reg.fit(a_prepared, axlab)
lin_reg

# Performans



#tahminlerini yap aslan modelim, modelimizin adı lin_reg hani yukarıda onu oluşturmuştuk
a_predictions = lin_reg.predict(a_prepared)
#şimdi modelin buldukları ile gerçek değerleri mukayese edelim:
lin_mse = mean_squared_error(axlab, a_predictions)
lin_rmse = np.sqrt(lin_mse)
lin_rmse


#learning curves
plot_learning_curves(lin_reg, a_prepared, y_prepared)
plt.axis([0, 80, 0, 3])                         # not shown in the book
#save_fig("underfitting_learning_curves_plot")   # not shown
plt.show()  

#--------------------------------------------

#SGD deneyelim
sgd_reg = SGDRegressor(max_iter=50, penalty=None, eta0=0.001, random_state=42)
sgd_reg.fit(a_prepared, y_prepared.ravel())
#performans
a_predictions = sgd_reg.predict(a_prepared)
#şimdi modelin buldukları ile gerçek değerleri mukayese edelim:
mse = mean_squared_error(axlab, a_predictions)
rmse = np.sqrt(mse)
sgd_performans=rmse
sgd_performans

#sgd_performans graf
plot_learning_curves(sgd_reg, a_prepared, y_prepared)
plt.axis([0, 80, 0, 3])                         # not shown in the book
#save_fig("underfitting_learning_curves_plot")   # not shown
plt.show()  


#PolyNomial Regression

from sklearn.preprocessing import PolynomialFeatures
poly_features = PolynomialFeatures(degree=2, include_bias=False)
X_poly = poly_features.fit_transform(a_prepared)
std_scaler = StandardScaler()
lin_reg = LinearRegression()
polynomial_regression = Pipeline([
            ("poly_features", poly_features),
            ("std_scaler", std_scaler),
            ("lin_reg", lin_reg),
        ])
pol_reg=polynomial_regression.fit(a_prepared, y_prepared)



a_predictions = pol_reg.predict(a_prepared)
#şimdi modelin buldukları ile gerçek değerleri mukayese edelim:
mse = mean_squared_error(axlab, a_predictions)
rmse = np.sqrt(mse)
pol_performans=rmse
pol_performans

#overfit underfit kontrolu
from sklearn.pipeline import Pipeline


plot_learning_curves(pol_reg, a_prepared, y_prepared)
plt.axis([0, 80, 0, 3])           # not shown
save_fig("learning_curves_plot")  # not shown
plt.show()                        # not shown

