import pandas as pd
a=pd.read_csv("C:\\Users\\suata\\Desktop\\scikit-ogren\\jupyter\\data\\train.csv")
display(a.head(2))
#casual,registered ve datetimeyi silelim
silinecek_kolonlar=['casual','datetime','registered']
a=a.drop(silinecek_kolonlar,axis=1)

# Definition of the CategoricalEncoder class, copied from PR #9151.
# Just run this cell, or copy it to your code, do not try to understand it (yet).

from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.utils import check_array
from sklearn.preprocessing import LabelEncoder
from scipy import sparse
import numpy as np
import imp as im
%matplotlib inline
import matplotlib
import matplotlib.pyplot as plt

dosyaYolu="C:\\Users\\suata\\Desktop\\scikit-ogren\\rodeo\\deneylerim\\bisiklet\\"

ce= im.load_source('module.name', dosyaYolu+'CategoricalEncoder.py')
dfs=im.load_source('module.name', dosyaYolu+'DataFrameSelector.py')
lecu=im.load_source('module.name', dosyaYolu+'LearningCurves.py')

#etiketsiz veri
axnolab=a.drop('count',axis=1)
#etiketler
axlab=a['count']

#kategorik ve nümerik kolonların sınıflandırılması
categorical_columns=['season','holiday','weather']
a_num = axnolab.drop(categorical_columns, axis=1)
a_ctg=axnolab[categorical_columns]
numerical_columns=list(a_num)


#pipeline dönüşüm tezgahı
from sklearn.pipeline import Pipeline
#feature scaler dönütürücüsü
from sklearn.preprocessing import StandardScaler

num_pipeline = Pipeline([
        ('selector', dfs.DataFrameSelector(numerical_columns)),
        ('std_scaler', StandardScaler()),
    ])

cat_pipeline = Pipeline([
        ('selector', dfs.DataFrameSelector(categorical_columns)),
        ('cat_encoder', ce.CategoricalEncoder(encoding="onehot-dense")),
    ])


#verinin hazır hale getirilmesi

from sklearn.pipeline import FeatureUnion

#pipeline'larımız
full_pipeline = FeatureUnion(transformer_list=[
        ("num_pipeline", num_pipeline),
        ("cat_pipeline", cat_pipeline),
    ])
#hadi gaza bas:))) dönüşüm yap
a_prepared = full_pipeline.fit_transform(axnolab)

y_prepared=np.array(axlab)

