#Model
from sklearn.datasets import fetch_mldata
mnist = fetch_mldata('MNIST original')
X,y=mnist['data'],mnist['target']
X_train=X[:60000]
x_test= X[60000:]
y_train=y[:60000]
y_test= y[60000:]
#training data üzerinden shuffle_index
np.random.seed(1983)
shuffle_index=np.random.permutation(60000)
X_train=X_train[shuffle_index]
y_train=y_train[shuffle_index]

#Stokastik Gradyan Deseni
from sklearn.linear_model import SGDClassifier
sgd_clf=SGDClassifier(random_state=1983)
sgd_clf.fit(X_train,y_train_5)
#model hazır modelin gücüne training data üzerinden bakalım, henüz test sette deneme yapmadık
#acc
from sklearn.model_selection import cross_val_score
cross_val_score(sgd_clf,X_train,y_train_5,cv=3,scoring="accuracy")
#conf matrix
from sklearn.model_selection import  cross_val_predict
#5 olup olmadıığı
y_train_pred=cross_val_predict(sgd_clf,X_train,y_train_5,cv=3)
from sklearn.metrics import confusion_matrix
confusion_matrix(y_train_5,y_train_pred)
#prec
from sklearn.metrics import precision_score
precision_score(y_train_5,y_train_pred)
#recall
from sklearn.metrics import recall_score
recall_score(y_train_5,y_train_pred)