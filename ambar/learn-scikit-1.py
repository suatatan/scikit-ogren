import pandas as pd
import random as ra
ra.seed(42)
x_1=ra.sample(range(1, 100), 10)
x_2=ra.sample(range(1, 100), 10)

a=pd.Series(x_1)
b=pd.Series(x_2)
c=pd.Series(['yes','no','yes','yes','yes','yes','yes','yes','no','no'])
#a function for our 'secret' model
y=2*a+3*b+5
y[3]=None
y[5]=None
# let's add change some values with None's to our data
df=pd.DataFrame(data=dict(a=a, b=b,c=c,y=y), index=a.index)

#scatter matrix
from pandas.tools.plotting import scatter_matrix
attr=["a","b","y"]
scatter_matrix(df[attr],figsize=(12,8))

#correl
df.corr()

#conv cat to val
from sklearn.preprocessing  import LabelEncoder
encoder=LabelEncoder()
c_encoded=encoder.fit_transform(df['c'])
#Let's assume we do not know the our y model and find it with regression via sci kit


